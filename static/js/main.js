function accessToggle() {
    document.getElementById("access-element").classList.toggle("access-height");
    document.getElementById("nav-list").classList.toggle("hidden-class");

}

$('#nav-list li').on('click', function() {
    $('#nav-list li').not(this).find('div').hide();
    $(this).find('div').toggle();
});

function sidebar() {
    document.getElementById("nav-toggle").classList.toggle("max-width-aside");
    document.getElementById("nav-content").classList.toggle("d-block")
}

function passwordSelect() {
    let x = document.getElementById("password-select").value;
    if (x == 1){
        document.getElementById("password-change").style.display = "block "
    }
    else{
        document.getElementById("password-change").style.display = "none "
    }
}
function picSelect() {
    let x = document.getElementById("pic-select").value;
    if (x == 1 ){
        document.getElementById("pic-change").style.display = "block "
    }
    else{
        document.getElementById("pic-change").style.display = "none "
    }
}


// checked all check box

$('.selectall').click(function() {
    if ($(this).is(':checked')) {
        $('input:checkbox').prop('checked', true);
    } else {
        $('input:checkbox').prop('checked', false);
    }
});

$("input[type='checkbox'].justone").change(function(){
    var a = $("input[type='checkbox'].justone");
    if(a.length == a.filter(":checked").length){
        $('.selectall').prop('checked', true);
    }
    else {
        $('.selectall').prop('checked', false);
    }
});


// moving content in admin setting

$( function() {
  $( "#sortable1" ).sortable({
    connectWith: ".connectedSortable"
  }).disableSelection();
} );


// chart
Chart.defaults.global.defaultFontFamily = 'vazir';
var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
  type: 'line',
  data: {
    labels: ['فروردین', 'اردیبهشت', 'خرداد', 'تیر', 'مرداد', 'شهریور','مهر','آبان','آذر','دی','بهمن','اسفند'],
    datasets: [{
      label: 'تعداد نظرات',
      data: [12, 19, 30, 5, 16, 9,23 ,15 , 10, 9 ,27,22 ],
      backgroundColor: [
        'rgba(125,0,163,0.2)',

      ],
      borderColor: [
        'rgb(125,0,163)',

      ],
      borderWidth: 1
    }]
  },
  options: {

    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    }
  }
});

// tooltip
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();
});

// tags
function newTextBox(element){
  if(!element.value){
    element.parentNode.removeChild( element.nextElementSibling);
    return;}
  else if(element.nextElementSibling)
    return;
  var newTxt = element.cloneNode();
  newTxt.id = 'txt_'+( parseInt( element.id.substring(element.id.indexOf('_')+1)) + 1);
  newTxt.value='';
  element.parentNode.appendChild(newTxt);
}
//slug

